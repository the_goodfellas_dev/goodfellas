/**
 * Decorator: TGFRoute
 * @author Victor Potasso <victorpotasso@gmail.com>
 */

var Router = require('../services/router');
var modules = {};

module.exports = function TGFRoute(routes) {
    return function(target) {
        target.prototype.router = Router.instance();
        var i = 0;
        var _routes = {};
        while(i < routes.length) {
            modules[routes[i].url] = routes[i];
            _routes[routes[i].url] = callback;
            i++;
        }
        target.prototype.router.on(_routes).resolve();
    }
}

/**
 * Router callback
 */
function callback(event) {
    var module = modules[event.route];
    var views = module.views;
    views.map(function(view) {
        try {
            var context = document.querySelector('view[id="' + view.id + '"]');
            var Klass = view.module;
            var instance = new Klass(event.params);
            context.appendChild(instance._el);
        }
        catch (err) {
            console.error(err);
        }
    })
}
