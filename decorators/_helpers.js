module.exports = {
    buildModules: buildModules,
    buildControllers: buildControllers,
    buildDependencies: buildDependencies
}

/**
 * Build modules
 */
function buildModules(context, modules) {
    var _modules = new Array();
    for(var module in modules) {
        var Klass = modules[module];
        var instance = new Klass();
        context.appendChild(instance._el);
        _modules[module] = instance;
    }
    return _modules;
}

/**
 * Build controllers
 */
function buildControllers(view, controllers) {
    var _controllers = new Array();
    for(var controller in controllers) {
        var Klass = controllers[controller];
        var instance = new Klass(view);
        _controllers[controller] = instance;
    }
    return _controllers;
}

/**
 * Build Dependencies
 */
function buildDependencies(target, dependencies) {
    var _dependencies = new Array();
    for(var dependency in dependencies) {
        var Klass = dependencies[dependency];
        var instance = Klass.instance();
        target.prototype[Klass.prototype._name] = instance;
        _dependencies[Klass.prototype._name] = target.prototype[Klass.prototype._name];
    }
    return _dependencies;
}
