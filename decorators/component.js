/**
 * Decorator: TGFComponent
 * @author Victor Potasso <victorpotasso@gmail.com>
 */

var helpers = require('./_helpers');

module.exports = function TGFComponent(params) {
    if(!params.selector) throw new Error('[TGFComponent decorator] Parameter "selector" is null.');

    return function(target) {
        // Element
        var el = document.createElement(params.selector);
        el.id = params.id;

        // Parent
        if(params.parent) document.body.appendChild(el);

        // Template
        if(params.template) el.innerHTML = params.template;

        // Modules
        if(params.modules) target.prototype.modules = helpers.buildModules(el, params.modules);
        target.prototype.buildModules = helpers.buildModules;

        // Controllers
        if(params.controllers) target.prototype.controllers = helpers.buildControllers(el, params.controllers);
        target.prototype.buildControllers = helpers.buildControllers;

        // Dependencies
        if(params.dependencies) target.prototype.dependencies = helpers.buildDependencies(target, params.dependencies);
        target.prototype.buildDependencies = helpers.buildDependencies;

        target.prototype._el = el;
    }
}
