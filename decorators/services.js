/**
 * Decorator: TGFServices
 * @author Victor Potasso <victorpotasso@gmail.com>
 */

var helpers = require('./_helpers');

module.exports = function TGFServices(services) {
    return function(target) {
        if(services) target.prototype.services = helpers.buildDependencies(target, services);
    }
}
